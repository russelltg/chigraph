#ifndef CHIG_NODE_TYPE_HPP
#define CHIG_NODE_TYPE_HPP

#pragma once

#include "chig/json.hpp"

#include <iterator>
#include <utility>

#include <llvm/IR/Value.h>
#include <llvm/IR/Function.h>
#include <llvm/IR/IRBuilder.h>
#include <llvm/IR/Instructions.h>
#include <llvm/IR/Module.h>


namespace chig {

struct Context;

// generic type
struct NodeType {

	NodeType(Context& con) : context{&con} {}
	
	virtual ~NodeType() = default;

	std::string name;
	std::string module;
	std::string description;

	Context* context;
	
	// inputs and outputs
	std::vector<std::pair<llvm::Type*, std::string>> inputs;
	
	std::vector<std::pair<llvm::Type*, std::string>> outputs;
	
	unsigned int numOutputExecs = 1;

	/// A virtual function that is called when this node needs to be called
	/// \param io This has the values that are the inputs and outputs of the function. 
	/// This vector will always have the size of `inputs.size() + outputs.size()` and starts with the inputs.
	/// The types are gaurenteed to be the same as inputs and outputs
	/// \param codegenInto The IRBuilder object that is used to place calls into
	/// \param outputBlocks The blocks that can be outputted. This will be the same size as numOutputExecs.
	virtual void codegen(const std::vector<llvm::Value*>& io, llvm::IRBuilder<>* codegenInto, const std::vector<llvm::BasicBlock*>& outputBlocks) const = 0;
	virtual nlohmann::json toJSON() const {return {};}
	
	/// Clones the type
	///
	virtual std::unique_ptr<NodeType> clone() const = 0;
	
};

struct FunctionCallNodeType : NodeType {

	FunctionCallNodeType(Context& context, llvm::Module* argModule, llvm::Function* func, int num_inputs, int numExecOutputs, std::string argDescription, const std::vector<std::string>& iodescs) : NodeType(context), function{func} {
		
		module = argModule->getName();
		
		numOutputExecs = numExecOutputs;
		
		name = func->getName();
		description = std::move(argDescription);

		// populate inputs and outputs
		inputs.resize(num_inputs);
		auto beginningOfOutptus = func->getArgumentList().begin();
		std::advance(beginningOfOutptus, num_inputs);
		
		std::transform(func->getArgumentList().begin(), beginningOfOutptus, iodescs.begin(), inputs.begin(), 
			[](auto& arg, auto& desc){return std::make_pair(arg.getType(), desc);});

		int num_outputs = std::distance(func->getArgumentList().begin(), func->getArgumentList().end()) - num_inputs;

		outputs.resize(num_outputs);
		std::transform(beginningOfOutptus, func->getArgumentList().end(), iodescs.begin() + num_inputs, outputs.begin(), [](auto& arg, auto& desc){return std::make_pair(arg.getType(), desc);});

	}

	FunctionCallNodeType(const FunctionCallNodeType&) = default;
	FunctionCallNodeType(FunctionCallNodeType&&) = default;
	
	llvm::Function* function;

	virtual void codegen(const std::vector<llvm::Value*>& io, llvm::IRBuilder<>* codegenInto, const std::vector<llvm::BasicBlock*>& outputBlocks) const override {

		auto ret = codegenInto->CreateCall(function, io);
		
		// we can optimize with a unconditional jump
		if(numOutputExecs == 1) {
			
			codegenInto->CreateBr(outputBlocks[0]);
			
		} else {
			
			auto sw = codegenInto->CreateSwitch(ret, outputBlocks[0], numOutputExecs); 
			
			for(size_t i = 0; i < outputBlocks.size(); ++i) {
				sw->addCase(llvm::ConstantInt::get(llvm::IntegerType::get(llvm::getGlobalContext(), 32), i), outputBlocks[i]);
			}
		}

		
	}
	
	virtual std::unique_ptr<NodeType> clone() const override {
		return std::make_unique<FunctionCallNodeType>(*this);
	}

};

}

#endif // CHIG_NODE_TYPE_HPP
